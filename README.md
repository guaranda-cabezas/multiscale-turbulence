# multiscale-turbulence
Repository of the **SC**ale **I**nvariant **Diff**usion (**ScIDiff**) model, which generates Lagrangian trajectories.

This work was developed under an internship at LISN, in 2023.

Student: Maria Guaranda  
Supervisors: 
- Guillaume Charpiat
- Sergio Chibbaro
- Lionel Mathelin

### Credits
Part of the source code, to be specific, the training pipeline, is heavily inspired on the [DiffWave repository](https://github.com/lmnt-com/diffwave).

## Getting started
1. Clone the repository
```
git remote add origin https://gitlab.lisn.upsaclay.fr/guaranda-cabezas/multiscale-turbulence.git
```
2. Create a ```./data``` folder (root directory).
3. Optionally, create an ```./experiments``` folder.
4. Create a ```params.yaml``` file or edit the one that is already given in this repository, under the ```src/``` folder.

### To run an experiment:
```
source some_environment/bin/activate
python src/main.py path/to/model_dir_or_checkpoint path/to/dataset -p path/to/params.yaml
```

```-p``` is an optional argument; if not specified, the parameters for the experiment will be taken from the ```src/params.yaml``` file 

### Logging the loss and gradient norm
During training, a tensorflow file will be generated/updated. This file is a log that contains the values of the loss and gradient norms. An example of how this file is named is: ```events.out.tfevents.1691158416.margpu008.713172```

To check the loss and gradient norm **during training** (in Tensorboard only):
```
# open another terminal to monitor
tensorboard --logdir /path/to/model/dir --bind_all
```
To see the training loss and gradient norm curves **after training**:
```
cd /path/to/model/dir
tensorboard dev upload --logdir . \
    --name "(optional) My latest experiment" \
    --description "(optional) Simple comparison of several hyperparameters"
```

### To generate data:
Usage:
```
python src/infer.py path/to/model_dir_or_checkpoint -o path/to/store/data \
-n 32 -p ./src/params.yaml
```
Example:
```
python src/infer.py ./experiments/20230815/1349393/ -o ./experiments/20230815/1349393/generated_samples_3d.npy -n 16384 -p ./experiments/20230815/1349393/params.yaml --normalized
```

- model_dir: path to the directory that contains a file named ```weights.pt```
- -o: path to the output file. Defaults to ```output.npy```. **Note** you have to specify the filename and its extension, which is npy.
- -n: number of samples to generate. For now, it should be a factor of 2, e.g. 2^12.
- -p: path to the params.yaml file that corresponds to the model used for inference. Defaults to ```./params.yaml```
- --normalized: indicates that the training data was normalized, so during sampling, the script applies reverse normalization on the generated samples. Default is False.

## Repository structure
- ```./docs```: contains some pdfs of the literature review carried  on during the internship, as well as the internship report and oral presentation.
- ```./notebooks```: Jupyter notebooks with mainly exploratory analysis of the data (original and generated samples).
- ```./src```: contains all the source code.
    - ```/data```: contains some utilities to process the dataset and compute statistics.
    - Files with the ```.job``` extensions are scripts used by Slurm.

## Trained models
There is a ```experiments.tar.xz``` compressed folder, which contains the experiments carried out during the internship. The experiments have been organized in two levels: there are folders with dates as names; this is the date when the experiment was performed. Inside them, there are folders with numbers; these are the JOB IDS given by Slurm when batching the training of the models. Example:

```bash
├── 20230821
│   ├── 1359824
        ├── weights.pt
```

All the checkpoints or weights of the models have a ```.pt``` extension. 

For the report, we presented the results of the following models:
- ScIDiff-tanh: 20230804/1321869/weights.pt
- ScIDiff-att: 20230815/1349393/weights.pt

The results of the baseline (DiffWave) can be found in this other [repository](https://gitlab.lisn.upsaclay.fr/guaranda-cabezas/diffwave)