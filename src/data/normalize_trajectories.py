import sys
import numpy as np

if __name__ == "__main__":
    # receives as argument the path to the dataset file
    filename = sys.argv[1]
    x_train = np.load(filename, encoding="ASCII", allow_pickle=True, mmap_mode='r')
    rx0, rx1 = np.amin(x_train, axis=(0,1)), np.amax(x_train, axis=(0,1))
    print("rx0: ", rx0)
    print("rx1: ", rx1)

    arr_shape = x_train.shape
    dst = filename[:-4]+"_scaled.npy"
    # scale
    x_train = 2*(x_train-rx0)/(rx1-rx0) - 1
    # save the minimum and maximum values of the dataset as a numpy arrays
    np.save(filename[:-4]+"_min.npy", rx0)
    np.save(filename[:-4]+"_max.npy", rx1)
    np.save(dst, x_train)
    sys.exit(0)