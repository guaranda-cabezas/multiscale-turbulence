from cProfile import label
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from fast_histogram import histogram1d
from sklearn.neighbors import KernelDensity
from scipy.stats import norm

class StatisticalPhysics:

    def __init__(self, trajectory):
        """Class that computes particle statistics

        Args:
            trajectory (numpy array): coolection of trajectories that hold into a 3d array of dimension (N, 2000,3)
        """
        self.trajectory = trajectory
        self.bw_adjust = 1
    
    def compute_delta_tau_velocity(self, tau, coordinate=-1):
        """For a collection of particle trajectory it computes delta_tau_V
        Since flow is anisotropic an average is done over all dimensions.

        Args:
            tau (int): time step difference to compute delta tau velocity
            coordinate (int): dimension along which to compute the delta velocity.
            If it's -1, all the dimensions or coordinates are going to be used, and 
            an average of velocity will be computed.
        """
        
        # we only have 3 dimensions or coordinates: x,y and z (0, 1 or 2)
        assert coordinate >= -1 and coordinate <=2 and type(coordinate) == int

        if coordinate==-1:
            v_x, v_y, v_z = (
                self.trajectory[:, :, 0],
                self.trajectory[:, :, 1],
                self.trajectory[:, :, 2],
            )

            delta_vx, delta_vy, delta_vz = (
                np.apply_along_axis(self.compute_array_difference, 0, v_x, tau),
                np.apply_along_axis(self.compute_array_difference, 0, v_y, tau),
                np.apply_along_axis(self.compute_array_difference, 0, v_z, tau),
            )
            # dividing by 3 because it's an average over coordinates x, y and z (3 dimensions)
            return np.add(np.add(delta_vx, delta_vy), delta_vz) / 3
        else:
            v = self.trajectory[:, :, coordinate]
            delta_v = np.apply_along_axis(self.compute_array_difference, 1, v, tau)
            return delta_v
            

    def compute_delta_tau_velocity_for_pdf(self, tau, coordinate=-1):

        # we only have 3 dimensions or coordinates: x,y and z (0, 1 or 2)
        assert coordinate >= -1 and coordinate <=2 and type(coordinate) == int

        if coordinate==-1:
            v_x, v_y, v_z = (
                self.trajectory[:, :, 0],
                self.trajectory[:, :, 1],
                self.trajectory[:, :, 2],
            )

            delta_vx, delta_vy, delta_vz = (
                np.apply_along_axis(self.compute_array_difference, 0, v_x, tau),
                np.apply_along_axis(self.compute_array_difference, 0, v_y, tau),
                np.apply_along_axis(self.compute_array_difference, 0, v_z, tau),
            )
            delta_vx_square, delta_vy_square, delta_vz_square = (
                np.apply_along_axis(self.compute_square_row_wise, 0, delta_vx),
                np.apply_along_axis(self.compute_square_row_wise, 0, delta_vy),
                np.apply_along_axis(self.compute_square_row_wise, 0, delta_vz),
            )

            normalizing_delta_vx, normalizing_delta_vy, normalizing_delta_vz = (
                delta_vx / np.sqrt(delta_vx_square.mean()),
                delta_vy / np.sqrt(delta_vy_square.mean()),
                delta_vz / np.sqrt(delta_vz_square.mean()),
            )
            return (
                np.add(
                    np.add(normalizing_delta_vx, normalizing_delta_vy), normalizing_delta_vz
                )
                / 3
            )
        else:
            v = self.trajectory[:, :, coordinate]
            delta_v = np.apply_along_axis(self.compute_array_difference, 1, v, tau)
            delta_v_square = np.apply_along_axis(self.compute_square_row_wise, 1, delta_v)
            norm_factor = np.sqrt(delta_v_square.mean())
            normalizing_delta_v = delta_v / norm_factor
            return normalizing_delta_v

    def plot_pdf_delta_tau_velocity(self, tau, coordinate=-1, ax=None, clip=(-40,40), y_log=False):
        """For a given tau plot the pdf delta_tau_velocity

        Args:
            tau(int): delta time.
            coordinate(int): dimension along which to compute the delta velocity.If it's -1, all the dimensions or 
            coordinates are going to be used, and an average of velocity will be computed.
            ax ([matplotlib], optional): Given matplotlib axis in order to overlap plots on same display. Defaults to None.
            clip(tuple): tuple of two values that represent the range of values to be plotted. Defaults to (-40,40).
            y_log(bool): if True, the y axis will be in log scale. Defaults to False.
        """
        norm_delta_tau_velocity = self.compute_delta_tau_velocity_for_pdf(tau, coordinate).flatten()
        kde_sk = KernelDensity(bandwidth=3, kernel='gaussian', atol=1e-8, rtol=1e-8)
        kde_sk.fit(norm_delta_tau_velocity.reshape([-1, 1]))
        max_val = np.max(norm_delta_tau_velocity)
        min_val = np.min(norm_delta_tau_velocity)
        ub = max_val if max_val < clip[1] else clip[1] 
        lb = min_val if min_val > clip[0] else clip[0] 

        eval_points = np.linspace(lb, ub, 10000)
        y_sk = np.exp(kde_sk.score_samples(eval_points.reshape(-1, 1)))

        if ax is None:
            plt.plot(eval_points, y_sk,  label=r"$\tau=$" + str(tau))
            if y_log:
                plt.title("Normalized pdf of delta_v with lin-log scale")
                plt.yscale("log")
            else:
                plt.title("Normalized pdf of delta_v with lin-lin scale")
                plt.show()
            
        else:
            ax.plot(eval_points, y_sk,  label=r"$\tau=$" + str(tau))

    def plot_pdf_delta_tau_velocity_legacy(self, tau, coordinate= -1, ax=None):
        """For a given tau plot the pdf delta_tau_velocity

        Args:
            tau ([int]):
            ax ([matplotlib], optional): Given matplotlib axis in order to overlap plots on same display. Defaults to None.
        """
        norm_delta_tau_velocity = self.compute_delta_tau_velocity(tau, coordinate).flatten()
        sns.kdeplot(
            data=norm_delta_tau_velocity,
            label=r"$\tau$=" + str(tau),
            log_scale=(False, True),
            ax=ax,
        )
        
        if ax is None:
            plt.ylabel("pdf : " + r"$\delta_{\tau}V$")
            plt.xlabel(
            r"$\delta_{\tau} \mathrm{V} /\left\langle\left(\delta_{\tau} \mathrm{V}\right)^{2}\right\rangle^{1 / 2}$")
            plt.legend()
            plt.show()


    def plot_several_tau_delta_velocity(self, list_taus, coordinate=-1, clip=(-40,40), y_log=False, legacy=False):
        if len(list_taus) > 1:
            _, ax = plt.subplots()
        ax.set_ylabel("pdf : " + r"$\delta_{\tau}V$")
        ax.set_xlabel(
            r"$\delta_{\tau} \mathrm{V} /\left\langle\left(\delta_{\tau} \mathrm{V}\right)^{2}\right\rangle^{1 / 2}$"
        )
        for tau in list_taus:
            if legacy:
                self.plot_pdf_delta_tau_velocity_legacy(tau, coordinate, ax=ax)
            else:
                self.plot_pdf_delta_tau_velocity(tau, coordinate, ax=ax, clip=clip, y_log=y_log)
        
        plt.legend()
        if y_log:
            ax.set_title("Normalized pdf of delta_v with lin-log scale")
            ax.set_yscale("log")
        else:
            ax.set_title("Normalized pdf of delta_v with lin-lin scale")
        plt.show()

    def compute_lagrangian_structure_function(self, n, coordinate=-1):
        """ Computes the Lagrangian structure function, given by:
            
        .. math::

        S_n(\tau) = \left \langle (\delta_{\tau}V)^n \right \rangle

        Args:
            n (int): the nth moment or the exponent.
            coordinate (int): dimension along which to compute the delta velocity.
            If it's -1, all the dimensions or coordinates are going to be used, and 
            an average of velocity will be computed.

        Returns:
            tau (numpy.ndarray): an array with integers that represent a range of tau 
            values.
            sn_tau (numpy.ndarray): array that contains the structure function values per each 
            value of tau.
        """
        # tau values from 1 to 1000
        tau = np.unique([int(t) for t in np.logspace(0, 3)])
        # compute structure function
        # the order of operations is: per each tau value, compute the delta tau velocity, 
        # compute the nth power of each element,
        # and take the mean
        sn_tau = np.array(
            [
                np.mean(
                    np.apply_along_axis(
                        self.compute_nth_power_row_wise,
                        1,
                        self.compute_delta_tau_velocity(tau=int(t), coordinate=coordinate),
                        n,
                    ).flatten()
                )
                for t in tau
            ]
        )
        return tau, sn_tau

    def compute_flatness_function(self, n, coordinate=-1):
        tau, sn_tau = self.compute_lagrangian_structure_function(n, coordinate)
        _, sndiv2_tau = self.compute_lagrangian_structure_function(2, coordinate)
        sndiv2_tau = sndiv2_tau ** (n / 2)
        flatness = sn_tau / sndiv2_tau
        return tau, flatness

    def plot_lagrangian_structure_function(self, n, coordinate=-1, ax=None):
        """For a given n plot the lagragian structure function

        Args:
            tau ([int]):
            ax ([matplotlib], optional): Given matplotlib axis in order to overlap plots on same display. Defaults to None.
        """

        tau, sn_tau = self.compute_lagrangian_structure_function(n, coordinate)
        if ax is None:
            _, ax = plt.subplots()
            ax.set_ylabel(r"$S_n({\tau})$")
            ax.set_xlabel(r"$\tau$")
            ax.set_yscale("log")
            ax.set_xscale("log")
            
        ax.scatter(tau, sn_tau, label=r"n=" + str(n), marker='^')
  
    def plot_flatness_function(self, n, coordinate= -1, ax=None):
        """For a given n plot the flatness function

        Args:
            n ([int]): the nth moment.
            ax ([matplotlib], optional): Given matplotlib axis in order to overlap plots on same display. Defaults to None.
        """

        tau, sn_tau = self.compute_flatness_function(n, coordinate)
        if ax is None:
            _, ax = plt.subplots()
        ax.set_ylabel(r"$F_n({\tau})$")
        ax.set_xlabel(r"$\tau$")
        ax.set_yscale("log")
        ax.set_xscale("log")
        ax.plot(tau, sn_tau, label=r"n=" + str(n), marker='^')
           
    def plot_several_flatness_function(self, list_n, coordinate=-1, ax=None):
        if len(list_n) > 1:
            if(ax is None):
                _, ax = plt.subplots()
        for n in list_n:
            self.plot_flatness_function(n, coordinate, ax=ax)
        ax.set_ylabel(r"$F_n({\tau})$")
        ax.set_xlabel(r"$\tau$")
        ax.set_yscale("log")
        ax.set_xscale("log")
        plt.legend()
        #plt.show()

    def plot_several_lagrangian_structure_function(self, list_n, coordinate=-1, ax=None):
        if len(list_n) > 1:
            if(ax is None):
                _, ax = plt.subplots()
        ax.set_ylabel(r"$S_n({\tau})$")
        ax.set_xlabel(r"$\tau$")
        ax.set_yscale("log")
        ax.set_xscale("log")
        for n in list_n:
            self.plot_lagrangian_structure_function(n, coordinate, ax=ax)
        plt.legend()
        #plt.show()
    
    def plot_vx_norm_pdf(self, y_log=True, ax=None, **kwargs):   
        """
        Generates pdf of x coordinate of the velocity, normalized by its standard deviation.
        """
        if(ax is None):
            _, ax = plt.subplots(figsize=(15, 6))
        sigma = np.std(self.trajectory[:,:,0].flatten())
        # get frequency of each value
        hist = histogram1d(self.trajectory[:,:,0].flatten(), 1000, range=[-10,10])
        x_bin, bin_width = np.linspace(-10, 10, num=1000), 20/ 1000
        # to compute a pdf, we need to divide by the bin width and the accumulated sum of the histogram
        ax.scatter(x_bin/sigma,   sigma* hist/(bin_width* np.sum(hist)), marker="+", color="orange", **kwargs)

        if y_log :
            ax.set_title("Normalized pdf of v_x with log-lin scale")
            ax.set_yscale("log")

        else :
            ax.set_title("Normalized pdf of v_x with lin-lin scale")

        ax.set_ylabel(r"PDF($v_x$) * $\sigma(x)$")
        ax.set_xlabel(r"$V_x$ / $\sigma(x)$")
        #plt.show()

    def plot_vx_pdf(self, y_log=True, ax=None, **kwargs):   
        """
        Generates pdf of x coordinate of the velocity.
        """
        if(ax is None):
            _, ax = plt.subplots(figsize=(8, 6))
        hist = histogram1d(self.trajectory[:,:,0].flatten(), 1000, range=[-10,10])
        x_bin, bin_width = np.linspace(-10, 10, num=1000), 20/ 1000
        
        ax.scatter(x_bin,  hist/(bin_width* np.sum(hist)), marker="+", color="orange", **kwargs)

        if y_log :
            ax.set_title("Pdf of v_x with lin-log scale")
            ax.set_yscale("log")

        else :
            ax.set_title("Pdf of v_x with lin-lin scale")

        ax.set_ylabel(r"PDF($v_x$)")
        ax.set_xlabel(r"x")
        plt.show()

    def plot_ax_norm_pdf(self, y_log=False, ax=None, **kwargs):
        """
        Generates normalized pdf of x coordinate of the acceleration.
        """
        if(ax is None):
            _, ax = plt.subplots(figsize=(8, 6))
        a_x = np.gradient(self.trajectory[:, :, 0], axis=1)
        sigma = np.std(a_x.flatten())
        hist =  histogram1d(a_x.flatten(), 1000, range=[-20,20])
        x_bin, bin_width = np.linspace(-20, 20, num=1000), 40/ 1000
        ax.plot(x_bin/sigma, sigma* hist/(bin_width* np.sum(hist)), **kwargs)

        if y_log :
            ax.set_title("Normalized pdf of ax with log-lin scale")
            ax.set_yscale("log")
            ax.set_xlim([-100,100])

        else :
            ax.set_title("Normalized pdf of ax with lin-lin scale")
            #ax.set_xlim([-150,150])
        

        ax.set_ylabel(r"PDF($a_x$) * $\sigma(a_x)$")
        ax.set_xlabel(r"$a_x$ / $\sigma(a_x)$")

    @staticmethod
    def compute_array_difference(x, tau):
        """
        Computes, for a 1d array, l[t+tau]-t[t]. This is,
        \delta_{\tau}V.

        Args:
            tau ([int]):
        """
        if(type(tau == np.int64)):
            tau = int(tau) 
        assert tau > 0 and type(tau) == int and len(x.shape)==1
        return x[tau:] - x[:-tau]

    @staticmethod
    def compute_square_row_wise(x):
        """
        Computes, for a 1d array, its square (squares each element of the array)."""
        return np.square(x)

    @staticmethod
    def compute_nth_power_row_wise(x, n):
        """
        Computes, for a 1d array, the power of n of each element. 
        Example: 
        x = [2, 3,]
        n = 3
        compute_nth_power_row_wise(x, n)
        >>> [8, 27,]
        """
        return np.power(x, n)
