"""
This script is used to generate the plots for the report.
Command line arguments (pass them in this order):
    - path to save the figures
    - path to the generated data
    - path to the original data

Example:
    python src/generate_report_plots.py figs/ data/generated_data.npy data/original_data.npy
"""
import numpy as np
import matplotlib.pyplot as plt

import sys
import os

from data.lagrangian_datatools import *
import data.statistics as ds
import importlib
import seaborn as sns
import scienceplots
from matplotlib.lines import Line2D

# styles for the report
plt.style.use(['science','no-latex','nature'])
plt.rcParams.update({'figure.dpi': '200'}) 
plt.rcParams.update({'font.size': 14, "legend.fontsize":"small"})
plt.rcParams.update({"markers.fillstyle": "none", "lines.markersize": 3})
plt.rcParams.update({"lines.linewidth": .9})

colors =['#377eb8', '#ff7f00', '#4daf4a','#f781bf', '#a65628', '#984ea3',
        '#999999', '#e41a1c', '#dede00']
markers = ["o", "x", "s"]
plot_kwargs = {"capsize":2, "errorevery":2}

lines = [Line2D([0], [0], linestyle='solid', color='black'),
         Line2D([0], [0], linestyle='dashed', color='black')]
labels = ['Original', 'Generated']

# Get command line arguments
FIGS_PATH = sys.argv[1]
# Generated data
filename = sys.argv[2]
# Original data
orig_fname = sys.argv[3]

# comparing with the same number of points
# For the report, we used 10k samples of trajectories
N_GEN_DATA = 10000
SEED = 42
# Parameters for error bars
SUBSAMPLING_TIMES = 10

# sampling from the original data
np.random.seed(SEED)

# =============================================================================

orig_data = ParticleDataset(npy_filename=orig_fname, root_dir=".")
# subsample the original data to match in length to the generated data
idx = np.random.choice(327680, N_GEN_DATA, replace=False)
orig_stat = ds.StatisticalPhysics(orig_data[idx]) 

# Loading the data
d1 = ParticleDataset(npy_filename=filename, root_dir=".")

# one sample
sample_1 = d1[10]
plt.plot(np.arange(2000), sample_1[:,0])
plt.ylabel(r"$V_x(t)$")
plt.xlabel("t")
plt.savefig(os.path.join(FIGS_PATH, "sample_1.eps"), bbox_inches="tight", dpi=200)

stat = ds.StatisticalPhysics(d1)

# tau goes from 1 to 1000
list_taus = np.unique([int(t) for t in np.logspace(0, 3)]).astype(int)
list_n=[2,4,6]

## Lagrangian structures

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylim(bottom=10e-4, top=10e4)
ax.set_ylabel(r"$S_n({\tau})$", size=14)
ax.set_xlabel(r"$\tau$", size=14)
ax.set_yscale("log")
ax.set_xscale("log")

for i,n in enumerate(list_n):
    tau, sn_tau = stat.compute_lagrangian_structure_function(n, coordinate=0)
    ax.plot(tau, sn_tau, label=r"n=" + str(n), color=colors[i], linestyle="dashed")
    # Original data 
    tau, sn_tau = orig_stat.compute_lagrangian_structure_function(n, coordinate=0)
    ax.plot(tau, sn_tau, color=colors[i])
    ax.text(tau[25], sn_tau[30], r"n=" + str(n), size=8)

plt.legend(lines, labels, fontsize=7, loc="upper left")
plt.savefig(os.path.join(FIGS_PATH, "structures.eps"), bbox_inches="tight", dpi=200)

## Error bars with subsampling method

subsamples = []
structure_2 = {"values": [], "sd": None, "mean": None}
structure_4 = {"values": [], "sd": None, "mean": None}
structure_6 = {"values": [], "sd": None, "mean": None}

for _ in range(SUBSAMPLING_TIMES):
    subsample_idx = np.random.choice(len(d1), N_GEN_DATA, replace=True)
    subsample = d1[subsample_idx]
    subsamples.append(subsample)
    temp_stat = ds.StatisticalPhysics(subsample)
    for n in list_n:
        tau, fn_tau = temp_stat.compute_lagrangian_structure_function(n, coordinate=0)
        if n == 2:
            structure_2["values"].append(fn_tau)
        elif n == 4:
            structure_4["values"].append(fn_tau)
        elif n == 6:
            structure_6["values"].append(fn_tau)

# compute mean and standard error
# for each tau
# we can vertically stack the values and then compute the mean and std per column

structure_2["values"] = np.vstack(structure_2["values"])
structure_2["ci"] = np.percentile(structure_2["values"], [2.5, 97.5], axis=0)
structure_2["mean"] = np.mean(structure_2["values"], axis=0)
structure_2["sd"] = np.std(structure_2["values"], axis=0)
structure_2["se"] = structure_2["sd"] / np.sqrt(structure_2["values"].shape[0])

structure_4["values"] = np.vstack(structure_4["values"])
structure_4["ci"] = np.percentile(structure_4["values"], [2.5, 97.5], axis=0)
structure_4["mean"] = np.mean(structure_4["values"], axis=0)
structure_4["sd"] = np.std(structure_4["values"], axis=0)
structure_4["se"] = structure_4["sd"] / np.sqrt(structure_4["values"].shape[0])

structure_6["values"] = np.vstack(structure_6["values"])
structure_6["ci"] = np.percentile(structure_6["values"], [2.5, 97.5], axis=0)
structure_6["mean"] = np.mean(structure_6["values"], axis=0)
structure_6["sd"] = np.std(structure_6["values"], axis=0)
structure_6["se"] = structure_6["sd"] / np.sqrt(structure_6["values"].shape[0])

bootstrapped_stat = {2: structure_2, 4: structure_4, 6: structure_6}

### error bars with 95% confidence interval

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylabel(r"$S_n({\tau})$", size=14)
ax.set_xlabel(r"$\tau$", size=14)
ax.set_yscale("log")
ax.set_xscale("log")

for i,n in enumerate(list_n):
    ax.errorbar(tau, bootstrapped_stat[n]["mean"], yerr=bootstrapped_stat[n]["ci"], color=colors[i], linestyle="dashed", **plot_kwargs)
    # Original data
    tau, sn_tau = orig_stat.compute_lagrangian_structure_function(n, coordinate=0)
    ax.plot(tau, sn_tau, label=r"n=" + str(n), color=colors[i])
    ax.text(tau[25], sn_tau[30], r"n=" + str(n), size=8)

plt.legend(lines, labels, fontsize=7, loc="lower right")
plt.savefig(os.path.join(FIGS_PATH, "structures_ci.eps"), bbox_inches="tight", dpi=200)

### error bars with standard error
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylabel(r"$S_n({\tau})$", size=14)
ax.set_xlabel(r"$\tau$", size=14)
ax.set_yscale("log")
ax.set_xscale("log")

for i,n in enumerate(list_n):
    ax.errorbar(tau, bootstrapped_stat[n]["mean"], yerr=bootstrapped_stat[n]["se"], color=colors[i], linestyle="dashed", **plot_kwargs)
    # Original data
    tau, sn_tau = orig_stat.compute_lagrangian_structure_function(n, coordinate=0)
    ax.plot(tau, sn_tau, color=colors[i])
    ax.text(tau[25], sn_tau[30], r"n=" + str(n), size=8)

plt.legend(lines, labels, fontsize=7, loc="lower right")
plt.savefig(os.path.join(FIGS_PATH, "structures_se.eps"), bbox_inches="tight", dpi=200)

### error bars with standard deviation
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylabel(r"$S_n({\tau})$", size=14)
ax.set_xlabel(r"$\tau$", size=14)
ax.set_yscale("log")
ax.set_xscale("log")

for i,n in enumerate(list_n):
    ax.errorbar(tau, bootstrapped_stat[n]["mean"], yerr=bootstrapped_stat[n]["sd"], color=colors[i], linestyle="dashed", **plot_kwargs)
    # Original data 
    tau, sn_tau = orig_stat.compute_lagrangian_structure_function(n, coordinate=0)
    ax.plot(tau, sn_tau, color=colors[i])
    ax.text(tau[25], sn_tau[30], r"n=" + str(n), size=8)

plt.legend(lines, labels, fontsize=7, loc="lower right")
plt.savefig(os.path.join(FIGS_PATH, "structures_sd.eps"), bbox_inches="tight", dpi=200)

# Flatness

list_n=[4,6]

flatness_4 = {"values": [], "sd": None, "mean": None}
flatness_6 = {"values": [], "sd": None, "mean": None}
for _ in range(SUBSAMPLING_TIMES):
    temp_stat = ds.StatisticalPhysics(subsamples[i])
    for n in list_n:
        tau, fn_tau = temp_stat.compute_flatness_function(n, coordinate=0)
        if n == 4:
            flatness_4["values"].append(fn_tau)
        elif n == 6:
            flatness_6["values"].append(fn_tau)

flatness_4["values"] = np.vstack(flatness_4["values"])
flatness_4["ci"] = np.percentile(flatness_4["values"], [2.5, 97.5], axis=0)
flatness_4["mean"] = np.mean(flatness_4["values"], axis=0)
flatness_4["sd"] = np.std(flatness_4["values"], axis=0)
flatness_4["se"] = flatness_4["sd"] / np.sqrt(flatness_4["values"].shape[0])

flatness_6["values"] = np.vstack(flatness_6["values"])
flatness_6["ci"] = np.percentile(flatness_6["values"], [2.5, 97.5], axis=0)
flatness_6["mean"] = np.mean(flatness_6["values"], axis=0)
flatness_6["sd"] = np.std(flatness_6["values"], axis=0)
flatness_6["se"] = flatness_6["sd"] / np.sqrt(flatness_6["values"].shape[0])

bootstrapped_stat = {4: flatness_4, 6: flatness_6}

### error bars with 95% confidence interval

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylim(bottom=10e-1, top=10e4)
ax.set_ylabel(r"$F_n({\tau})$", size=14)
ax.set_xlabel(r"$\tau$", size=14)
ax.set_yscale("log")
ax.set_xscale("log")

for i,n in enumerate(list_n):
    ax.errorbar(tau, bootstrapped_stat[n]["mean"], yerr=bootstrapped_stat[n]["ci"], color=colors[i], linestyle="dashed", **plot_kwargs)
    # Original data will have markers '+'
    tau, fn_tau = orig_stat.compute_flatness_function(n, coordinate=0)
    ax.plot(tau, fn_tau, label=r"n=" + str(n), color=colors[i])
    ax.text(tau[16], fn_tau[11], r"n=" + str(n), size=8)

plt.legend(lines, labels, fontsize=7, loc="upper right")
plt.savefig(os.path.join(FIGS_PATH, "flatness_ci.eps"),bbox_inches="tight", dpi=200)

### error bars with standard error
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylim(bottom=10e-1, top=10e4)
ax.set_ylabel(r"$F_n({\tau})$", size=14)
ax.set_xlabel(r"$\tau$", size=14)
ax.set_yscale("log")
ax.set_xscale("log")

for i,n in enumerate(list_n):
    ax.errorbar(tau, bootstrapped_stat[n]["mean"], yerr=bootstrapped_stat[n]["se"], color=colors[i], linestyle="dashed", **plot_kwargs)
    # Original data will have markers '+'
    tau, fn_tau = orig_stat.compute_flatness_function(n, coordinate=0)
    ax.plot(tau, fn_tau,  color=colors[i])
    ax.text(tau[16], fn_tau[11], r"n=" + str(n), size=8)

plt.legend(lines, labels, fontsize=7, loc="upper right")
plt.savefig(os.path.join(FIGS_PATH, "flatness_se.eps"),bbox_inches="tight", dpi=200)

### error bars with standard deviation
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_ylim(bottom=10e-1, top=10e4)
ax.set_ylabel(r"$F_n({\tau})$", size=14)
ax.set_xlabel(r"$\tau$", size=14)
ax.set_yscale("log")
ax.set_xscale("log")

for i,n in enumerate(list_n):
    ax.errorbar(tau, bootstrapped_stat[n]["mean"], yerr=bootstrapped_stat[n]["sd"], color=colors[i], linestyle="dashed", **plot_kwargs)
    # Original data will have markers '+'
    tau, fn_tau = orig_stat.compute_flatness_function(n, coordinate=0)
    ax.plot(tau, fn_tau,  color=colors[i])
    ax.text(tau[16], fn_tau[11], r"n=" + str(n), size=8)

plt.legend(lines, labels, fontsize=7, loc="upper right")
plt.savefig(os.path.join(FIGS_PATH, "flatness_sd.eps"),bbox_inches="tight", dpi=200)

# PDF's
fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_xlim(-10, 10)
stat.plot_vx_norm_pdf(y_log=False, ax=ax)
# set font size for the x and y labels
ax.yaxis.get_label().set_fontsize(14)
ax.xaxis.get_label().set_fontsize(14)
# remove title when saving fig
ax.set_title("")
plt.savefig(os.path.join(FIGS_PATH, "vx_pdf.eps"),bbox_inches="tight", dpi=200)


## In log scale
fig = plt.figure()
ax = fig.add_subplot(111)
stat.plot_vx_norm_pdf(y_log=True, ax=ax)
# set font size for the x and y labels
ax.yaxis.get_label().set_fontsize(14)
ax.xaxis.get_label().set_fontsize(14)
# remove title when saving fig
ax.set_title("")
plt.savefig(os.path.join(FIGS_PATH, "vx_pdf_log.eps"),bbox_inches="tight", dpi=200)


## delta v pdf normalized
_, ax = plt.subplots()
ax.set_ylim(bottom=10e-6, top=1)
ax.set_ylabel("pdf : " + r"$\delta_{\tau}V$")
ax.set_xlabel(
    r"$\delta_{\tau} \mathrm{V} /\left\langle\left(\delta_{\tau} \mathrm{V}\right)^{2}\right\rangle^{1 / 2}$")
test_taus = [1,10,100]
for i, tau in enumerate(test_taus):
    # plot original data pdfs
    norm_delta_tau_velocity = orig_stat.compute_delta_tau_velocity_for_pdf(tau, coordinate=0).flatten()
    sns.kdeplot(
        data=norm_delta_tau_velocity,
        label=r"$\tau$=" + str(tau),
        log_scale=(False, True),
        ax=ax,
        cut=0,
        clip=(-20, 20),
        bw_adjust=1+i*2,
        linewidth=.8,
        color=colors[i],
    )
     # plot generated data pdfs
    norm_delta_tau_velocity = stat.compute_delta_tau_velocity_for_pdf(tau, coordinate=0).flatten()
    sns.kdeplot(
        data=norm_delta_tau_velocity,
        label=r"$\tau$=" + str(tau),
        log_scale=(False, True),
        ax=ax,
        cut=0,
        clip=(-20, 20),
        bw_adjust=1+i*2,
        linestyle="dotted",
        marker="s",
        linewidth=.8,
        markevery=10,
        markersize=4,
        markeredgewidth=0.5, color=colors[i],
    )
# hack for showing the color legend
ax2 = ax.twinx()
for i, tau in enumerate(test_taus):
    ax2.plot(np.NaN, np.NaN, color=colors[i],
             label=r"$\tau$=" + str(tau))
ax2.get_yaxis().set_visible(False)
# The curves of the generated data will now be dotted
lines[1] = Line2D([0], [0], linewidth=.8, linestyle='dotted', color='black')
ax.legend(lines, labels, fontsize=7, loc="upper left") #legend for lines styles
ax2.legend(loc=1, fontsize=7) 
# set font size for the x and y labels
ax.yaxis.get_label().set_fontsize(14)
ax.xaxis.get_label().set_fontsize(14)
# save fig
plt.savefig(os.path.join(FIGS_PATH, "delta_v_pdf.eps"), bbox_inches="tight", dpi=200)

## acceleration pdf normalized
# In log scale
fig = plt.figure()
ax = fig.add_subplot(111)
stat.plot_ax_norm_pdf(y_log=True, ax=ax, label="Generated", linestyle="dashed", color=colors[0])
orig_stat.plot_ax_norm_pdf(y_log=True, ax=ax, label="Original", linestyle="solid", color=colors[1])
ax.yaxis.get_label().set_fontsize(14)
ax.xaxis.get_label().set_fontsize(14)
ax.set_title("")
plt.legend(loc="upper left", fontsize=7)
# save fig
plt.savefig(os.path.join(FIGS_PATH, "ax_pdf_log.eps"), bbox_inches="tight", dpi=200)